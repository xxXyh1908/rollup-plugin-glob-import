<img alt="Version" src="https://img.shields.io/badge/version-2.3.0-blue.svg?cacheSeconds=2592000" />
<img alt="rollup" src="https://img.shields.io/badge/rollup-2.0.0-orange.svg?cacheSeconds=2592000" />
<img src="https://img.shields.io/badge/node-%3E%3D12.2.0-green.svg" />
<a href="#" target="_blank">
  <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-yellow.svg" />
</a>

# [🏠 @xyh19/rollup-plugin-glob-import](https://gitee.com/xxXyh1908/rollup-plugin-glob-import/#readme)

<blockquote>A rollup-plugin to use glob-star.</blockquote>

## install

```bash
npm install -D @xyh19/rollup-plugin-glob-import
# or
yarn add -D @xyh19/rollup-plugin-glob-import
```

## Getting Started

Create a `rollup.config.js` [configuration file](https://www.rollupjs.org/guide/en/#configuration-files) and import the plugin:

```js
// rollup.config.js
import globImport from '@xyh19/rollup-plugin-glob-import'
import path from 'path'

export default {
  input: 'src/index.js',
  output: {
    dir: 'output',
    format: 'cjs',
  },
  plugins: [
    globImport({
      globConfigs: [
        {
          loader: 'default',
          glob: 'src/components/*',
          aliases: '~components',
          dynamicImport: false,
          moduleIdRenamer: (filename) => path.parse(filename).name,
          moduleKeyRenamer: (filename) => path.parse(filename).name,
        },
        {
          loader: 'vue-routes',
          glob: 'src/page/**/*.vue',
          aliases: '~routes',
          moduleIdRenamer: (filename) => path.parse(filename).name,
          moduleKeyRenamer: (filename) => path.parse(filename).name,
        },
      ],
    }),
  ],
}
```

Import virtual module:

```js
// src/components/a.js
export default 'components-a';

// src/components/b.js
export default 'components-b';

// src/index.js
import * as components from '~components';
console.log(components) // { a: 'components-a', b: 'components-b'}
```

## Options

### `globConfigs`

Type: `GlobConfig | GlobConfig[]`<br>

glob-import configs.

### `exclude`

Type: `string | string[]`<br>
Default: `null`

A [minimatch pattern](https://github.com/isaacs/minimatch), or array of patterns, which specifies the files in the build the plugin should _ignore_.

### `defaultLoader`

Type: `'import' | 'default' | 'named' | 'namedModule' | 'mixed' | 'vue-routes' | 'react-routes' | Loader`<br>

A function generator for module export.

## GlobConfig

### `aliases`

Type: `string | string[]`<br>
Default: `null`

Provide the virtual ID of the imported module.

### `glob`

Type: `string`<br>

Glob expression to import.

### `exclude`

Type: `string | string[]`<br>
Default: `null`

A [minimatch pattern](https://github.com/isaacs/minimatch), or array of patterns, which specifies the files in the build the plugin should _ignore_.

### `transform`

Type: `TransformHook`<br>

## Show your support

Give a ⭐️ if this project helped you!
