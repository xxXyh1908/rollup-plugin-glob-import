declare module 'glob:*' {
  const modules: Record<string, any>
  export = modules
}

declare module 'glob-import:*' {}

declare module 'glob-default:*' {
  const modules: Record<string, any>
  export = modules
}

declare module 'glob-named:*' {
  const modules: Record<string, any>
  export = modules
}

declare module 'glob-namedModule:*' {
  const modules: Record<string, Record<string, any>>
  export = modules
}

declare module 'glob-mixed:*' {
  const modules: Record<string, any>
  export = modules
}

declare module 'glob-vue-routes:*' {
  // @ts-ignore
  import type { RouteRecordRaw } from 'vue-router'

  const routes: RouteRecordRaw[]
  export default routes

  type VueRouteRaw = Omit<RouteRecordRaw, 'component' | 'components' | 'children' | 'name'> & {
    component?: string
    children?: VueRouteRaw[]
    name: string
    [x: keyof any]: any
  }

  export const current: VueRouteRaw
  export const { component, children, name, path }: VueRouteRaw
}

declare module 'glob-react-routes:*' {
  // @ts-ignore
  import type { RouteObject } from 'react-router'

  const routes: RouteObject[]
  export default routes

  type ReactRouteRaw = {
    caseSensitive?: boolean
    children?: ReactRouteRaw[]
    element?: string
    index?: boolean
    path: string
    [x: keyof any]: any
  }

  export const current: ReactRouteRaw
  export const { element, children, index, path }: ReactRouteRaw
}
